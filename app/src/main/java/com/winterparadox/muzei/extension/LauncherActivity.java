package com.winterparadox.muzei.extension;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by Adeel on 9/22/2015.
 */
public class LauncherActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        openApp(this, "net.nurik.roman.muzei");
    }

    /** Open another app.
     * @param context current Context, like Activity, App, or Service
     * @param packageName the full package name of the app to open
     * @return true if likely successful, false if unsuccessful
     */
    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);



        if (i != null && i.resolveActivity(manager)!=null) {
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);
            Toast.makeText(context, "Please customize the Muzei appp and set Deadpool as source.",
                    Toast.LENGTH_LONG).show();
        }else {
            String url4 = "market://details?id="+packageName;
            Intent i4 = new Intent(Intent.ACTION_VIEW);
            i4.setData(Uri.parse(url4));
            context.startActivity(i4);
            Toast.makeText(context, "Please install the Muzei Live Wallpaper to use deadpool.",
                    Toast.LENGTH_LONG).show();
        }
        return true;
    }
}
