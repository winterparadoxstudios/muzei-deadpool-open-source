package com.winterparadox.muzei.extension;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import com.google.android.apps.muzei.api.Artwork;
import com.google.android.apps.muzei.api.MuzeiArtSource;
import com.google.android.apps.muzei.api.UserCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Adeel on 9/2/2015.
 */
public class WallpaperSource extends MuzeiArtSource {

    static String SOURCE_NAME = "deadpool";
    static String INDEX_KEY = "index";
    static String JSON_FILE = "data.json";
    static String ARRAY_KEY = "wallpapers";
    int CUSTOM_COMMAND_FEEDBACK = MAX_CUSTOM_COMMAND_ID - 5;
    int CUSTOM_COMMAND_ABOUTUS = MAX_CUSTOM_COMMAND_ID - 10;
    int CUSTOM_COMMAND_RATEUS = MAX_CUSTOM_COMMAND_ID - 15;
    int DELAY = 1000 * 60 * 60 * 24; // A day

    public WallpaperSource (String name) {
        super(name);
    }
    public WallpaperSource () {
        super(SOURCE_NAME);
    }


    @Override
    protected void onUpdate(int reason) {

        publishNextArtwork();

        // Muzei Menu Items; Including the next button
        ArrayList<UserCommand> commands = new ArrayList<>();
        commands.add(new UserCommand(BUILTIN_COMMAND_ID_NEXT_ARTWORK));
        //commands.add(new UserCommand(CUSTOM_COMMAND_FEEDBACK, "Feedback"));
        commands.add(new UserCommand(CUSTOM_COMMAND_ABOUTUS, "About Us"));
        commands.add(new UserCommand(CUSTOM_COMMAND_RATEUS, "Rate Us"));

        setUserCommands(commands);
    }


    private void publishNextArtwork() {
        SharedPreferences prefs = getSharedPreferences();

        JSONArray wallpapers;
        Artwork art = null;

        int index = 0;
        try {
            // Load wallpapers from json file in assets
            wallpapers = new JSONObject(loadJSONFromAsset(JSON_FILE)).getJSONArray(ARRAY_KEY);

            // loads current number of wallpaper used or selects a random number for first install
            index = prefs.getInt(INDEX_KEY, ((int)(Math.random() * wallpapers.length())) - 1);

            // get the next wallpaper/artwork in the list
            index++;

            index = index % wallpapers.length();

            art = Artwork.fromJson(wallpapers.getJSONObject(index));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // and publish it
        publishArtwork(art);

        // and schedule the next wallpaper update
        scheduleUpdate(System.currentTimeMillis() + DELAY);

        // save the current index/number of wallpaper
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(INDEX_KEY, index);
        editor.apply();

        SharedPreferences ratePrefs = getSharedPreferences(
                getString(R.string.rating_prefs_name), MODE_PRIVATE);

        // check different conditions to show rating dialog
        if (index > 2 && !ratePrefs.getBoolean(DialogActivity.HAS_RATED, false)) {
            Intent i = new Intent(this, DialogActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }

    /**
        What should we do when a menu item is clicked
     */
    @Override
    protected void onCustomCommand(int id) {
        if (id == BUILTIN_COMMAND_ID_NEXT_ARTWORK) {
            publishNextArtwork();

        } else if (id == CUSTOM_COMMAND_FEEDBACK) { // this is not called
            Intent intent3 = new Intent(Intent.ACTION_SENDTO);
            intent3.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent3.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.email_address)});
            intent3.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
            intent3.putExtra(Intent.EXTRA_TEXT, getString(R.string.content_email));
            intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (intent3.resolveActivity(getApplication().getPackageManager()) != null) {
                startActivity(intent3);
            }

        } else if (id == CUSTOM_COMMAND_ABOUTUS) {
            String url = getString(R.string.content_web);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } else if (id == CUSTOM_COMMAND_RATEUS) {
            String url4 = "market://details?id="+getPackageName();
            Intent i4 = new Intent(Intent.ACTION_VIEW);
            i4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i4.setData(Uri.parse(url4));
            startActivity(i4);
        }
        super.onCustomCommand(id);
    }

    /** needs a json filename to load it as a string
    * @param name name of the json file
     */
    public String loadJSONFromAsset(String name) {
        String json;
        try {
            InputStream is = getApplicationContext().getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
