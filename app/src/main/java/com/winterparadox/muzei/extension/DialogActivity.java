package com.winterparadox.muzei.extension;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

/**
 * Created by Adeel on 9/22/2015.
 */
public class DialogActivity extends Activity {

    public static String HAS_RATED = "has_rated";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences prefs = getSharedPreferences(
                getString(R.string.rating_prefs_name), MODE_PRIVATE);
        if (!prefs.getBoolean(HAS_RATED, false))
        {
            new AlertDialog.Builder(this).setTitle(R.string.rate_us_title)
                    .setMessage(R.string.rate_us_message)
                    .setPositiveButton(R.string.btn_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putBoolean(HAS_RATED, true);
                            editor.apply();

                            String url4 = "market://details?id="+getPackageName();
                            Intent i4 = new Intent(Intent.ACTION_VIEW);
                            i4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i4.setData(Uri.parse(url4));
                            startActivity(i4);

                            finish();
                        }
                    })
                    .setNeutralButton(R.string.btn_maybe, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.btn_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putBoolean(HAS_RATED, true);
                            editor.apply();
                            finish();
                        }
                    })
                    .show();
        }

    }
}
