# README #

### What is this repository for? ###

* This is an open source version of our android app extension for Muzei Live Wallpaper App. [Here](https://play.google.com/store/apps/details?id=com.winterparadox.muzeideadpool) is the live version on play store. We have open sourced it so anyone can publish their Muzei extension with minimum coding required.
* The source code is from version 2.0 of our app.

### How do I get set up? ###

* Source code consist of an android studio gradle project so it should be easy to setup using android studio.
* All you need to do is add your own wallpaper links to the json file in the assets folder.
* You should change the values in the strings.xml, icons and package name of the project to customize name and branding of the app.
* Code is well commented and it should be easy to understand and configure for your own needs.

### Contribution guidelines ###

* We won't accept any contribution to this repo. But you are welcomed to fork it and change it however you like.



### Lisence ###

Lisence on this source code is Apache Lisence 2.0. A Lisence.txt file is also included in  the project.